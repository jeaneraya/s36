// Setup imports

const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoutes = require('./routes/taskRoutes.js')

// Express Setup
const app = express()
const port = 3001

// Initialize dotenv
dotenv.config()

app.use(express.json())
app.use(express.urlencoded({extended: true}))

// Mongoose Setup
mongoose.connect(`mongodb+srv://jeaneraya:admin123@cluster0.ehvmi0d.mongodb.net/s36-Activity?retryWrites=true&w=majority`, 
	{
	useNewUrlParser: true,
	useUnifiedTopology: true
	}
)


let db = mongoose.connection
db.on('error', () => console.error('Connection Error.'))
db.once('open', () => console.log('Connected to MongoDB!'))


// Routes
app.use('/tasks', taskRoutes)
// NOTE: Make sure to assign a specific endpoint for every database collection. In this case, since we are manipulating the 'tasks' collection in MongoDb, then we are specifying an endpoint with specific routes and controllers for that collection.



// Listen to port
app.listen(port, () => console.log(`Server is running at port: ${port}`))
