const express = require('express')

const TaskController = require('../controllers/TaskController.js')

const router = express.Router()


router.get('/', (req, res) => {
	TaskController.getAllTasks().then((resultFromController) => res.send(resultFromController))
})


router.post('/create', (req, res) =>{
	TaskController.createTask(req.body).then((resultFromController) => res.send(resultFromController))
})


// ACTIVITY - Route for getting specific task /tasks/:id
router.get('/:id', (req, res) => {
	TaskController.retrieveTaskById(req.params.id).then((resultFromController) => res.send(resultFromController))
})

// ACTIVITY - Route for tasks/complete
router.put('/:id/complete', (req, res) => {
	TaskController.updateTaskToCompleted(req.params.id, req.body).then((resultFromController) => res.send(resultFromController))
})

module.exports = router
