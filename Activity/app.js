const express = require('express')
const mongoose = require('mongoose')

const taskRoutes = require('./routes/taskRoutes.js')

const app = express()
const port = 3001


app.use(express.json())
app.use(express.urlencoded({extended: true}))

mongoose.connect(`mongodb+srv://jeaneraya:admin123@cluster0.ehvmi0d.mongodb.net/s36-Activity?retryWrites=true&w=majority`, 
	{
	useNewUrlParser: true,
	useUnifiedTopology: true
	}
)


let db = mongoose.connection
db.on('error', () => console.error('Connection Error.'))
db.once('open', () => console.log('Connected to MongoDB!'))


app.use('/tasks', taskRoutes)


app.listen(port, () => console.log(`Server is running at port: ${port}`))
